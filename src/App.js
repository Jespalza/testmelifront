import { BrowserRouter, Route, Routes, Redirect } from "react-router-dom";
import { SearchsComponent } from "./Components/Searchs/SearchComponent";
import { ListProductsView } from "./views/listProductsView";
import { DetailProductView } from "./views/detailProductView";
import { Error } from "./Components/messages/Error";
import { SearchMsn } from "./Components/messages/SearchMsn";

function App() {
  return (
    <BrowserRouter>
      <SearchsComponent />
      <Routes>
        <Route path='/' element={<SearchMsn />} />
        <Route path='/items' element={<ListProductsView />} />
        <Route path="/items/:id" element={<DetailProductView />} />
        <Route path="*" element={<Error />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
