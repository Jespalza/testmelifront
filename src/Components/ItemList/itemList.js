import React from 'react'
import PropTypes from 'prop-types'
import { Link } from "react-router-dom";
import './itemList.scss'
import Currency from "react-currency-format";



const ItemList = props => {
    return (
        <Link to={{ pathname: `/items/${props.id}` }} className="link">
            <div className='card_img'>
                <img className='img' src={props.image} alt={props.tags} />
            </div>
            <div>
                <p className='p'>
                    <Currency
                        className='price'
                        value={props.price}
                        prefix={'$ '}
                        thousandSeparator={true}
                        displayType={'text'}
                    />
                </p>
                <span className='title_item'>
                    {props.title}
                </span>
            </div>
            <div>
                <span className='address'>
                    {props.address}
                </span>
            </div>

        </Link>
    )
}

ItemList.propTypes = {
    price: PropTypes.number,
    image: PropTypes.string,
    title: PropTypes.string,
    address:PropTypes.string
}

export default ItemList