import { render } from "@testing-library/react";
import ItemList from "./itemList";

jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'),
    Link: (
        { children, ...rest }) =>
        <a data-testid="link" {...rest} >{children}</a>
})
);

describe("itemList", () => {
    it("init", () => {
        const item = {
            "id": "MLA931455240",
            "title": "Apple iPhone 11 (128 Gb) - Blanco",
            "price": {
                "currency": "ARS",
                "amount": 233504
            },
            "picture": "http://http2.mlstatic.com/D_796892-MLA46114829828_052021-I.jpg",
            "condition": "new",
            "free_shipping": true,
            "address": {
                "state_id": "AR-C",
                "state_name": "Capital Federal",
                "city_id": "TUxBQlBBTDI1MTVa",
                "city_name": "Palermo"
            }
        }
        const { container } = render(<ItemList
            id={item.id}
            price={item.price.amount}
            title={item.title}
            address={item.address.state_name}
            image={item.picture} />);
        expect(container).toMatchSnapshot();
    })
})