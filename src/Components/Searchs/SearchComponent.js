import React, { useState } from "react";
import logoMl from "../../assets/img/Logo_ML.png";
import "./Searchs.scss";
import { FaSearch } from "react-icons/fa";
import { useNavigate } from "react-router-dom";

export const SearchsComponent = () => {
  const [textSearch, setTextSearch] = useState("");
  let navigate = useNavigate()

  const handlerSubmit = (event) => {
    event.preventDefault();
    if (textSearch) {
      navigate(`/items/?search=${textSearch}`)
    }
  };

  const handlerPress = (event) => {
    if(event.key === 'Enter'){
      handlerSubmit(event);
    }
  }
  return (
    <div className="navbar">
      <div className="navbar_search">
        <div>
          <a href="/">
            <img className="logoML" src={logoMl} alt="Logo Mercado Libre" />
          </a>
        </div>
        <div>
          <input
          className="inputSeachs"
            type="text"
            placeholder="Nunca dejes de buscar"
            onChange={(e) => setTextSearch(e.target.value)}
            onKeyPress={(e)=> handlerPress(e)}
          ></input>
        </div>
        <div>
          <button className="btn_search" onClick={handlerSubmit}>
            <FaSearch />
          </button>
        </div>
      </div>
    </div>
  );
};
