import { render } from "@testing-library/react";
import { SearchsComponent } from "./SearchComponent";

jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'),
    useNavigate: jest.fn(() => 'bar')
}));

describe('searchsComponent', () => {
    it('init', () => {
        const component = render(<SearchsComponent />)
        expect(component).toMatchSnapshot
    })

    it('should keypress enter', () => {
        const onKeyPress = jest.fn();
        const event = {
            key: "enter",
            preventDefault() { },
            target: { value: 'nevera' }
        }
        const component = render(<SearchsComponent onKeyPress={()=>onKeyPress(event)} />)
        expect(component).toMatchSnapshot();
    })

    it('should change enter', () => {
        const onChange = jest.fn();
        const event = {
            key: "enter",
            preventDefault() { },
            target: { value: 'iphone' }
        }
        const component = render(<SearchsComponent onChange={()=>onChange(event)} />)
        expect(component).toMatchSnapshot();
    })
})