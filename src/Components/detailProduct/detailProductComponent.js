import React from 'react'
import CurrencyFormat from 'react-currency-format';
import './detailProductComponent.scss'

export const DetailProductComponent = ({ product }) => {
    console.log(product);
    const condi = product.item.condition.charAt(0).toUpperCase() + product.item.condition.slice(1)
    return (

        <div className='cardDetails'>
            <div className='div_image'>
                <img src={product?.item.picture[0].url} alt='MercadoLibre' />
            </div>
            <div className='info'>
                <div className='condition'>
                    {condi} - {product.item.sold_quantity} Vendidos
                </div>
                <div className='title'>
                    {product.item.title}
                </div>
                <div>
                    <CurrencyFormat
                        value={product.item.price.amount}
                        className='priceDetails'
                        prefix={'$ '}
                        thousandSeparator={true}
                        displayType={'text'}
                    />
                </div>
                <button className='btn_comprar'> Comprar</button>
            </div>
            <div className='detailDescription'>
                <p className='titleDescription'>
                    Descripción del producto
                </p>
                <div className='productDescription'>
                    {product.item.description.plain_text}
                </div>
            </div>
        </div>


    )
}

