import { render } from "@testing-library/react";
import { DetailProductComponent } from "./detailProductComponent";

describe("detailProduct", () => {
    it('init', () => {
        const product = {
            "author": {
                "name": "Juan Manuel",
                "lastname": "Espalza Mora"
            },
            "item": {
                "id": "MLA1178087588",
                "title": "Tubo Termorretractil De Aislamiento Electrico 560 Piezas",
                "price": {
                    "currency": "ARS",
                    "amount": 7149
                },
                "picture": [
                    {
                        "id": "707942-MLA51702140395_092022",
                        "url": "http://http2.mlstatic.com/D_707942-MLA51702140395_092022-O.jpg",
                        "secure_url": "https://http2.mlstatic.com/D_707942-MLA51702140395_092022-O.jpg",
                        "size": "407x500",
                        "max_size": "1345x1649",
                        "quality": ""
                    },
                    {
                        "id": "884017-MLA51702140392_092022",
                        "url": "http://http2.mlstatic.com/D_884017-MLA51702140392_092022-O.jpg",
                        "secure_url": "https://http2.mlstatic.com/D_884017-MLA51702140392_092022-O.jpg",
                        "size": "500x403",
                        "max_size": "546x441",
                        "quality": ""
                    },
                    {
                        "id": "743595-MLA51702140393_092022",
                        "url": "http://http2.mlstatic.com/D_743595-MLA51702140393_092022-O.jpg",
                        "secure_url": "https://http2.mlstatic.com/D_743595-MLA51702140393_092022-O.jpg",
                        "size": "500x500",
                        "max_size": "500x500",
                        "quality": ""
                    },
                    {
                        "id": "846271-MLA51702140394_092022",
                        "url": "http://http2.mlstatic.com/D_846271-MLA51702140394_092022-O.jpg",
                        "secure_url": "https://http2.mlstatic.com/D_846271-MLA51702140394_092022-O.jpg",
                        "size": "500x500",
                        "max_size": "500x500",
                        "quality": ""
                    },
                    {
                        "id": "983140-MLA51702140390_092022",
                        "url": "http://http2.mlstatic.com/D_983140-MLA51702140390_092022-O.jpg",
                        "secure_url": "https://http2.mlstatic.com/D_983140-MLA51702140390_092022-O.jpg",
                        "size": "500x500",
                        "max_size": "500x500",
                        "quality": ""
                    },
                    {
                        "id": "669689-MLA51702140391_092022",
                        "url": "http://http2.mlstatic.com/D_669689-MLA51702140391_092022-O.jpg",
                        "secure_url": "https://http2.mlstatic.com/D_669689-MLA51702140391_092022-O.jpg",
                        "size": "500x500",
                        "max_size": "500x500",
                        "quality": ""
                    }
                ],
                "condition": "new",
                "free_shipping": true,
                "sold_quantity": 0,
                "description": {
                    "text": "",
                    "plain_text": "- ANTES DE COMPRAR PREGUNTE FECHA DE ENTREGA.\r\n- ENVIAMOS POR MERCADOENVIOS\r\n- PUEDE RETIRAR POR AHORA SOLO POR QUILMES, MICROCENTRO ESTA CERRADO, POR ESO...\r\n- EN CABA (CAPITAL FEDERAL) ENVIAMOS SIN CARGO ESTE PRODUCTO.\r\n- FORMA DE PAGO : MERCADOPAGO\r\n- HACEMOS FACTURA A.\r\n- ELBAZARDIGITAL VENDEDOR PLATINUM\r\n- TODOS NUESTROS PRODUCTOS EN:\r\n\r\nhttps://eshops.mercadolibre.com.ar/elbazardigital\r\n\r\n-X-X-X-\r\n\r\n- SOMOS IMPORTADORES DIRECTOS, ESTE PRODUCTO SE COMPRA Y SE IMPORTA DESDE ESTADOS UNIDOS, ESTO IMPLICA QUE USTED ESTA COMPRANDO EL MISMO PRODUCTO QUE COMPRARÍA UN CLIENTE DE ESE PAÍS.\r\n\r\n- ANTES DE REALIZAR UNA CONSULTA, VISUALICE TODAS LAS IMAGENES DEL PRODUCTO.\r\n\n- Titulo de Catalogo Original : \n560pcs Heat Shrink Tubing 2:1, Eventronic Electrical Wire Cable Wrap Assortment Electric Insulation Heat Shrink Tube Kit With Box(5 Colors/12 Sizes)\n\n- material: hecho de poliolefina, relacion de contraccion: 2: 1 (se encogera a la mitad de su diametro suministrado)\r\n\r\n- adecuado para aislamiento electrico, agrupacion de cables, codificacion por colores, proteccion mecanica, empalmes de cables/alambres y reparaciones diarias, etc.\r\n\r\n- viene con 560 piezas de tubos termorretractiles surtidos en caja de plastico, 5 colores (amarillo, azul, negro, verde, rojo), 12 especificaciones, satisfechas para sus diferentes necesidades\r\n\r\n- longitud: 1 13/16, diametro interno: 1/24 (90 piezas), 1/16 (70 piezas), 1/12 (70 piezas), 1/10 (50 piezas), 1/8 (50 piezas) , 1/7 (50 piezas), 1/6 (50 piezas), 1/5 (30 piezas), 1/4 (30 piezas), 2/7 (25 piezas), 2/5 (25 piezas), 1 /2 (20 piezas)\r\n\r\n- paquete incluido: 1 x 560pcs (12 tamaños) 2: 1 tubo termorretractil\r\n\r\n\r\n***para obtener los mejores resultados, utilice una pistola de aire caliente hasta que se reduzca de tamaño.\r\npaquete: (cantidad x diametro interno x longitud unica) 90 piezas x 1,0 mm x 45 mm negro 70 piezas x 1,5 mm x 45 mm rojo 80 piezas x 2,0 mm x 45 mm amarillo 70 piezas x 2,5 mm x 45 mm verde 50 piezas x 3,0 mm x 45 mm negro 45 piezas x 3,5 mm x 45 mm azul 45 piezas x 4,0 mm x 45 mm rojo 20 piezas x 5,0 mm x 45 mm azul 20 piezas x 6,0 mm x 45 mm azul 25 piezas x 7,0 mm x 45 mm rojo 25 piezas x 10,0 mm x 45 mm negro 20 piezas x 13,0 mm x 45 mm negro\r\n\r\nnota: temperatura minima de contraccion: 70c temperatura total de contraccion: 110c maxima temperatura de funcionamiento: -55c a 125c resistencia a la traccion: 10,4mpa resistencia dielectrica: 15 kv / mm inflamabilidad: retardante de llama metodo de corte: tijeras o cuchillo afilado\n\n-o-o-o-",
                    "last_updated": "2022-09-26T22:42:37.378Z",
                    "date_created": "2022-09-26T22:42:37.378Z",
                    "snapshot": {
                        "url": "http://descriptions.mlstatic.com/D-MLA1178087588.jpg?hash=8520c3b8559cb08aa7e782b8f5334ffe_0x0",
                        "width": 0,
                        "height": 0,
                        "status": ""
                    }
                }
            }
        }
        const {container} = render(<DetailProductComponent product={product}/>)
        expect(container).toMatchSnapshot()
    })
})