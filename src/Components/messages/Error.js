import React from 'react'
import imgError from "../../assets/img/error.svg";
import './error.scss'


export const Error = () => {
  return (
    <div className='divImgError'>
        <p className='titleError'>Parece que esta página no existe</p>
        <img className='imgError' src={imgError}/>
    </div>
  )
}
