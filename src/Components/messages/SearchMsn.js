import React from 'react'
import imgSearch from "../../assets/img/searchs.svg";
export const SearchMsn = () => {
    return (
        <div className='divImgError'>
            <p className='titleError'>Busca el producto que tanto quieres.</p>
            <img className='imgError' src={imgSearch} />
        </div>
    )
}
