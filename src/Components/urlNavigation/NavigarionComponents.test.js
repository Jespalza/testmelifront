import { render } from "@testing-library/react";
import  NavigationComponents  from "./NavigationComponents";

jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'),
    useLocation: () => {
        return {
            pathname: '/items',
            search: 'iphone',
        }
    }
}));

describe("NavigationComponent", () => {
    const StorageMock = {
        getItem: jest.fn(),
        setItem: jest.fn(),
    };

    global.localStorage = StorageMock;

    it('init', () => {
        const monk = {
            "categories": [
                "Celulares y Teléfonos",
                "Celulares y Smartphones"
            ]
        }
        const { container } = render(<NavigationComponents categories={monk.categories} />)
    })
})
