import React, { useState } from 'react'
import { useLocation } from 'react-router-dom'
import './NavigationComponents.scss'
const NavigationComponents = ({categories}) => {
    const { search } = useLocation()
    let item = new URLSearchParams(search).get('search')
    if (localStorage.getItem('breadcrumbs')) {
        categories = JSON.parse(localStorage.getItem('breadcrumbs'));
        item = localStorage.getItem('search');
    }

        localStorage.setItem('search', item);
        localStorage.setItem('breadcrumbs', JSON.stringify(categories));


    return (
        <div className='nav'>
            {
                categories.length > 0 ?
                    categories.map(
                        (cat) => {
                            return `${cat} > `
                        }
                    )
                    : ''
            }{item}
        </div>
    )
}

export default NavigationComponents