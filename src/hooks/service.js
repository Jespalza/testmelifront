export const UseFetchsGet = async (endpoint, value) => {
    const urlApiML = {
        products: `http://localhost:3002/api/items/?q=${value}`,
        descrip: `http://localhost:3002/api/items/${value}`,
    }

    const resp = await fetch(urlApiML[endpoint]);
    const data = await resp.json()
    return data;
}