import { UseFetchsGet } from "./service";


describe("ServiceFetch", () => {
    const mockRes = {
        "author": {
            "name": "Juan Manuel",
            "lastname": "Espalza Mora"
        },
        "categories": [
            "Celulares y Teléfonos",
            "Celulares y Smartphones"
        ],
        "items": [
            {
                "id": "MLA931455240",
                "title": "Apple iPhone 11 (128 Gb) - Blanco",
                "price": {
                    "currency": "ARS",
                    "amount": 233504
                },
                "picture": "http://http2.mlstatic.com/D_796892-MLA46114829828_052021-I.jpg",
                "condition": "new",
                "free_shipping": true,
                "address": {
                    "state_id": "AR-C",
                    "state_name": "Capital Federal",
                    "city_id": "TUxBQlBBTDI1MTVa",
                    "city_name": "Palermo"
                }
            },
            {
                "id": "MLA931172973",
                "title": "Apple iPhone 11 (64 Gb) - Negro",
                "price": {
                    "currency": "ARS",
                    "amount": 208499
                },
                "picture": "http://http2.mlstatic.com/D_656548-MLA46114829749_052021-I.jpg",
                "condition": "new",
                "free_shipping": true,
                "address": {
                    "state_id": "AR-C",
                    "state_name": "Capital Federal",
                    "city_id": "TUxBQlBBTDI1MTVa",
                    "city_name": "Palermo"
                }
            },
            {
                "id": "MLA1119599957",
                "title": "Apple iPhone 12 (128 Gb) - (product)red",
                "price": {
                    "currency": "ARS",
                    "amount": 373659.16
                },
                "picture": "http://http2.mlstatic.com/D_918849-MLA45719523826_042021-I.jpg",
                "condition": "new",
                "free_shipping": true,
                "address": {
                    "state_id": "AR-C",
                    "state_name": "Capital Federal",
                    "city_id": "TUxBQkFHUjk3NjJa",
                    "city_name": "Agronomía"
                }
            },
            {
                "id": "MLA1119444141",
                "title": "Apple iPhone SE (2da Generación) 64 Gb - Blanco",
                "price": {
                    "currency": "ARS",
                    "amount": 197999
                },
                "picture": "http://http2.mlstatic.com/D_745945-MLA46552310508_062021-I.jpg",
                "condition": "new",
                "free_shipping": true,
                "address": {
                    "state_id": "AR-C",
                    "state_name": "Capital Federal",
                    "city_id": "TUxBQkFHUjk3NjJa",
                    "city_name": "Agronomía"
                }
            }
        ]
    }

    it("searchs products", async () => {
        const fetchMonck = Promise.resolve({ json: () => Promise.resolve(mockRes) });
        global.fetch = jest.fn().mockImplementation(() => fetchMonck)

        const response = await UseFetchsGet("products", "iphone")
        expect(response).toEqual(mockRes)
    })
})