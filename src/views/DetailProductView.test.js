import { render } from "@testing-library/react"
import { UseFetchsGet } from "../hooks/service"
import { DetailProductView } from "./detailProductView"

describe("DetailProductView", () => {
    it("init",async () => {
        const {container} = render(<DetailProductView/>)
        expect(container).toMatchSnapshot();
    })
})