import { render } from "@testing-library/react"
import { ListProductsView } from "./listProductsView"

jest.mock('react-router-dom', () => ({
    ...jest.requireActual('react-router-dom'),
    useLocation: () => {
        return {
            pathname: '/items',
            search: 'iphone',
        }
    }
}));

describe("ListProductView",()=>{
    it("init",()=>{
        const {container} = render(<ListProductsView/>)
        expect(container).toMatchSnapshot()
    })
})