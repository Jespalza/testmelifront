import React, { useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { DetailProductComponent } from '../Components/detailProduct/detailProductComponent'
import NavigationComponents from '../Components/urlNavigation/NavigationComponents'
import { UseFetchsGet } from '../hooks/service'

export const DetailProductView = () => {

    const { id } = useParams()
    const [Product, setProduct] = useState()
    const [loading, setLoading] = useState(false)
    useEffect(() => {
        searchProductDetail()
    },[id])


    const searchProductDetail = async () => {
        const data = await UseFetchsGet('descrip', id)
        console.log("product", data);
        if(data){
            setProduct(data)
            setLoading(true)
        }
    }
    return (loading &&
        <div style={{ margin: '0 15%' }}>
            <NavigationComponents/>
            <DetailProductComponent product={Product} />
        </div>
    )
}
