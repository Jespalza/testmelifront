import React, { useEffect, useState } from 'react'
import { useLocation, } from 'react-router-dom';
import ItemList from "../Components/ItemList/itemList";
import NavigationComponents from '../Components/urlNavigation/NavigationComponents';
import { UseFetchsGet } from '../hooks/service';

export const ListProductsView = () => {
    let { search } = useLocation();
    const [Products, setProducts] = useState()
    const item = new URLSearchParams(search).get('search')

    useEffect(() => {
        SearchProductsList()
    }, [item])

    const SearchProductsList = async () => {
        const data = await UseFetchsGet('products', item)
        console.log("product", data);
        setProducts(data)
    }
    return (Products?.items.length > 0 &&
        <>
            <div style={{ margin: '0 15%' }}>
                <NavigationComponents categories={Products.categories} />
                {
                    Products?.items.map((product, index) => {
                        return <ItemList
                            key={index}
                            id={product.id}
                            price={product.price.amount}
                            title={product.title}
                            address={product.address.state_name}
                            image={product.picture}
                        />
                    })
                }
            </div>
        </>
    )
}

